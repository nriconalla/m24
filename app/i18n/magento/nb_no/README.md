# Magento 2 Language Pack

Auto translated to nb_NO with microsoft translate API's. May contain lot of spelling mistakes.

Why? We must start somewhere.

## How to use ?

### Install

1. Open up magento root
1. Require the language pack. `composer require pkj/magento2-nb_no:dev-master`
1. Clear cache. `bin/magento cache:clean`

### Upgrade to latest

1. `composer update pkj/magento2-nb_no`
1. Clear cache. `bin/magento cache:clean`


## Contribute

Send pull requests on the nb_no.csv file.




